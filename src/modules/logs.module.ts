import { Module, HttpModule } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { LogsService } from '../services/logs.service';
import { LogsController } from '../controllers/logs.controller';
import { Log, LogSchema } from '../schemas/log.schema';

@Module({
  imports: [
    MongooseModule.forFeature(
      [
        {
          name: Log.name,
          schema: LogSchema
        }
      ]
    ),
  ],
  controllers: [
    LogsController
  ],
  providers: [
    LogsService
  ]
})
export class LogsModule {}
