import { Module, HttpModule } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from '../controllers/app.controller';
import { AppService } from '../services/app.service';
import { CurrenciesModule } from './currencies.module';
import { LogsModule } from './logs.module';
import { Log, LogSchema } from '../schemas/log.schema';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: '.env'
    }),
    MongooseModule.forRoot('mongodb://localhost:27017/pulpoline-currency-converter'),
    MongooseModule.forFeature(
      [
        {
          name: Log.name,
          schema: LogSchema
        }
      ]
    ),
    HttpModule,
    LogsModule,
    CurrenciesModule
  ],
  controllers: [
    AppController
  ],
  providers: [
    AppService
  ]
})
export class AppModule {}
