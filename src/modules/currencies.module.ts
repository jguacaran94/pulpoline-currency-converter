import { Module, HttpModule } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CurrenciesService } from '../services/currencies.service';
import { CurrenciesController } from '../controllers/currencies.controller';
import { Log, LogSchema } from '../schemas/log.schema';

@Module({
  imports: [
    MongooseModule.forFeature(
      [
        {
          name: Log.name,
          schema: LogSchema
        }
      ]
    ),
    HttpModule,
  ],
  controllers: [
    CurrenciesController
  ],
  providers: [
    CurrenciesService
  ]
})
export class CurrenciesModule {}
