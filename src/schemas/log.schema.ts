import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type LogDocument = Log & Document;

@Schema()
export class Log {
  @Prop({ required: true })
  currencyIn: string;
  @Prop({ required: true })
  currencyOut: string;
  @Prop({ required: true })
  ipAddress: string;
  @Prop({ required: true, default: new Date() })
  createdAt: string;
  @Prop()
  attempts: number
}

export const LogSchema = SchemaFactory.createForClass(Log);
