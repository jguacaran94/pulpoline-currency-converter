import { Controller, Get, Query, Req, Res, HttpStatus } from '@nestjs/common';
import { Request, Response } from 'express';
import { AppService } from '../services/app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  // GET: /?from=usd&to=eur
  async convertCurrency(
    @Req() request: Request,
    @Query() query: { from: string, to: string },
    @Res() response: Response
  ): Promise<object> {
    const ipAddress: string = request.ip;
    let results;
    await this.appService.convertCurrency(query, ipAddress).then((res) => {
      results = res;
    });
    if (results.status === 403) {
      return response.status(HttpStatus.FORBIDDEN).json(results);
    }
    return response.json({
      status: 200,
      data: results
    });
  }
}
