import { Controller, Get } from '@nestjs/common';
import { LogsService } from '../services/logs.service';

@Controller('logs')
export class LogsController {
  constructor(private readonly logsService: LogsService) {}

  @Get()
  // GET: /logs
  findAll() {
    return this.logsService.findAll();
  }
}
