import { Injectable, HttpService, HttpException, HttpStatus } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Log } from '../schemas/log.schema';

@Injectable()
export class AppService {
  constructor(
    @InjectModel('Log') private readonly logModel: Model<Log>,
    private readonly http: HttpService
  ) {}

  async convertCurrency(obj: { from: string, to: string }, ipAddress: string): Promise<object> {
    if (!obj.from || !obj.to) {
      throw new HttpException({
        status: HttpStatus.BAD_REQUEST,
        error: 'From and To parameters must be supplied.',
      }, HttpStatus.BAD_REQUEST);
    }
    const apiKey: string = process.env.API_KEY;
    const url: string = `https://free.currconv.com/api/v7/convert?q=${obj.from}_${obj.to},${obj.to}_${obj.from}&compact=ultra&apiKey=${apiKey}`;
    let response;
    await this.http.get(url).toPromise()
    .then(async (res) => {
      const data: object = res.data;
      const status: number = res.status;
      if (status === 200) {
        response = data;
        let attempts: number = 1;
        const log = await this.logModel.findOne({ ipAddress: ipAddress }).sort({ createdAt: -1 }).exec();
        let logAttempts: number;
        if (log) {
          logAttempts = attempts + log.attempts;
          const yesterday = new Date();
          yesterday.setDate(yesterday.getDate() - 1);
          if (logAttempts > 5 && yesterday.toDateString() !== log.createdAt) {
            response = {
              message: 'You have reached the limit convertion attempts per day.',
              status: 403
            }
            return response;
          }
        } else {
          logAttempts = attempts;
        }
        const setLog = new this.logModel({
          currencyIn: obj.from,
          currencyOut: obj.to,
          ipAddress: ipAddress,
          attempts: logAttempts,
          createdAt: new Date()
        });
        await setLog.save();
      }
    })
    .catch((err) => {
      const status: number = err.response.status;
      const data = err.response.data;
      if (status === 400) {
        response = data;
        throw new HttpException({
          status: HttpStatus.BAD_REQUEST,
          error: data.error,
        }, HttpStatus.BAD_REQUEST);
      }
      if (status === 500) {
        response = data;
        throw new HttpException({
          status: HttpStatus.INTERNAL_SERVER_ERROR,
          error: data.error,
        }, HttpStatus.INTERNAL_SERVER_ERROR);
      }
    });
    return response;
  }
}
