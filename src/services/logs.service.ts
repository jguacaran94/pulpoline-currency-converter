import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Log } from '../schemas/log.schema';

@Injectable()
export class LogsService {
  constructor(
    @InjectModel('Log') private readonly logModel: Model<Log>
  ) {}

  async findAll(): Promise<object> {
    const logs = await this.logModel.find().exec();
    return logs;
  }
}
