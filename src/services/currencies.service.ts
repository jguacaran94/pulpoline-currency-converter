import { Injectable, HttpService, HttpException, HttpStatus } from '@nestjs/common';

@Injectable()
export class CurrenciesService {
  constructor(private readonly http: HttpService) {}

  async findAll(): Promise<object> {
    const apiKey: string = process.env.API_KEY;
    const url: string = `https://free.currconv.com/api/v7/currencies?apiKey=${apiKey}`;
    let response: { status?: number, results?: any, error?: string };
    await this.http.get(url).toPromise()
    .then((res: { data: object, status: number }) => {
      const data: object = res.data;
      const status: number = res.status;
      if (status === 200) {
        response = data;
      }
    })
    .catch((err: { response: { status: number, data: { error: string } } }) => {
      const status: number = err.response.status;
      const data: { error: string } = err.response.data;
      if (status === 400) {
        response = data;
        throw new HttpException({
          status: HttpStatus.BAD_REQUEST,
          error: data.error,
        }, HttpStatus.BAD_REQUEST);
      }
      if (status === 500) {
        response = data;
        throw new HttpException({
          status: HttpStatus.INTERNAL_SERVER_ERROR,
          error: data.error,
        }, HttpStatus.INTERNAL_SERVER_ERROR);
      }
    });
    return response;
  }
}
